<?php

/**
 * Description of EmpleadoTodo
 *
 * @author Profesor Ramon
 */
class EmpleadoTodo implements Todo{
    
    public function adios(): string {
        return "Adios";
    }

    public function hola(): string {
        return "Hola";
    }

    public function salir(): string {
        return "saliendo";
    }

    public function calcularSueldo(): float {
        return 1000.0;
    }

    public function mostrarInformacion(): string {
        return "informacion";
    }

}
