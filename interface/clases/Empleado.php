<?php

/**
 * Description of Empleado
 *
 * @author Profesor Ramon
 */
class Empleado implements Trabajador, Persona{
    
    public function adios(): string {
        return "adios";   
    }

    public function hola(): string {
        return "hola";
    }

    public function calcularSueldo(): float {
        return 1000.00;
    }

    public function mostrarInformacion(): string {
        return "mis datos";
    }

    
}
