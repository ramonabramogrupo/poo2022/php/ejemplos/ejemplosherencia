<?php

interface Trabajador {
    public function calcularSueldo(): float;
    public function mostrarInformacion(): string;
}
