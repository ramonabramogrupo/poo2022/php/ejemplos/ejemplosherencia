<?php

class Usuario implements Persona{
    public function adios(): string {
       return "Hasta la vista";  
    }

    public function hola(): string {
        return "Bienvenidos ";
    }

}
