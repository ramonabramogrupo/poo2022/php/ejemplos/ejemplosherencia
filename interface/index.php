<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});


// new Persona(); // no se puede instanciar un interface

$usuario1=new Usuario();
var_dump($usuario1);
echo $usuario1->hola();

$empleado1=new Empleado();
echo $empleado1->hola();
echo $empleado1->calcularSueldo();

$empleado2=new EmpleadoTodo();
echo $empleado2->salir();
