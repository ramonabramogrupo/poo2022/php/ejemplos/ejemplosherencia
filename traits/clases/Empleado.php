<?php

/**
 * Description of Empleado
 *
 * @author Profesor Ramon
 */
class Empleado{
    use Base, Direccion;
    public float $sueldo;
    
   
    public function __construct(string $nombre,float $sueldo,string $poblacion) {
        $this->__constructBase($nombre, "", "");
        $this->__constructDireccion("", 1, "", "", $poblacion);
        $this->sueldo = $sueldo;
    }

}
