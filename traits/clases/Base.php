<?php

trait Base {
    public string $nombre;
    public string $fechaNacimiento;
    public string $apellidos;
    
    public function hablar(string $texto){
        return "digo {$texto}";
    }
    
    public function __constructBase(string $nombre, string $fechaNacimiento, string $apellidos) {
        $this->nombre = $nombre;
        $this->fechaNacimiento = $fechaNacimiento;
        $this->apellidos = $apellidos;
    }

}
