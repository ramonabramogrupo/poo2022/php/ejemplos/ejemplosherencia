<?php

trait Direccion {
    public string $tipo;
    public int $numero;
    public string $bloque;
    public string $puerta;
    public string $poblacion;
    private int $cp;
    
    public function getCp(): int {
        return $this->cp;
    }

    public function setCp(int $cp): void {
        $this->cp = $cp;
    }
       
    public function __constructDireccion(string $tipo, int $numero, string $bloque, string $puerta, string $poblacion) {
        $this->tipo = $tipo;
        $this->numero = $numero;
        $this->bloque = $bloque;
        $this->puerta = $puerta;
        $this->poblacion = $poblacion;
    }

}
