<?php

trait Edificio {
    public int $altura;
    public bool $ascensor;
    
    public function subir():string{
        return "subiendo";
    }
    
    public function bajar():string{
        return "bajando";
    }
}
