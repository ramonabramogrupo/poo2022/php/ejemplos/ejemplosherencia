<div style="border: 1px solid black;width:400px;height:50px;">
   Texto de ejemplo
</div>

<?php
use clases\texto\Div;
use clases\texto\Div_1;

// autocarga de clases
spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

$caja=new Div("Texto de ejemplo");

echo $caja;
echo $caja;

$caja1=new Div("Pedro Gomez",800);
echo $caja1;

$caja2=new Div_1("Hola clase",400);
echo $caja2;