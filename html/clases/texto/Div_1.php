<?php

namespace clases\texto;

class Div_1 {
    
    private string $texto="";
    private ?int $ancho=null;
    private ?int $alto=null;
    
    public function __construct(string $texto, ?int $ancho=null,?int $alto=null) {
        $this->texto = $texto;
        $this->ancho = $ancho;
        $this->alto=$alto;
    }
    
    public function __toString() {
       $fichero= file_get_contents('clases/texto/plantillaDiv.php'); 
       
       $borde="border:5px solid black;";
       $ancho=$this->getAncho();
       $alto=$this->getAlto();
       
       $estilo=$borde . $ancho . $alto;
       $contenido=$this->texto;
       
       return str_replace(
               ["{{STYLE}}","{{TEXTO}}"],
               [$estilo,$contenido],
               $fichero
               );
       
    }
    
    public function getAncho():string{
        if(is_null($this->ancho)){
            return "";
        }else{
            $resultado="width:{$this->ancho}px;";
            return $resultado;
        }
        
    }
    
    public function getAlto(): string {
        if(!isset($this->alto)){
            return "";
        }else{
            $resultado="height:{$this->alto}px;";
            return $resultado;
        }
    }



    
}


