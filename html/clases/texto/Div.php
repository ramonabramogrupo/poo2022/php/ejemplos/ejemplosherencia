<?php

namespace clases\texto;

class Div {
    
    private string $texto="";
    private ?int $ancho=null;
    private ?int $alto=null;
    
    public function __construct(string $texto, ?int $ancho=null,?int $alto=null) {
        $this->texto = $texto;
        $this->ancho = $ancho;
        $this->alto=$alto;
    }
    
    public function __toString() {
        $resultado="<div style=\"border:1px solid black;";
        $resultado.=$this->getAncho();
        $resultado.=$this->getAlto();
        $resultado.="\">";
        $resultado.=$this->texto;
        $resultado.="</div>";
        return $resultado;
    }
    
    public function getAncho():string{
        if(is_null($this->ancho)){
            return "";
        }else{
            $resultado="width:{$this->ancho}px;";
            return $resultado;
        }
        
    }
    
    public function getAlto(): string {
        if(!isset($this->alto)){
            return "";
        }else{
            $resultado="height:{$this->alto}px;";
            return $resultado;
        }
    }



    
}


