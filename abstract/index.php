<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

 // new Base(); // las clases abstractas no pueden instanciarse

$jorge=new Cliente(); // creo un objeto de tipo cliente
var_dump($jorge);
echo $jorge->hablar(); // llamo al metodo hablar


$silvia=new Humano("silvia",30);
var_dump($silvia);
echo $silvia->saludar();

$bigotes=new Perro();
var_dump($bigotes);
echo $bigotes->saludar();
