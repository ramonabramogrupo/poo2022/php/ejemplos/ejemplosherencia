<?php

class Perro extends Ser{
    
    public function despedir(): string {
        return "Guau...";
    }

    public function saludar(): string {
        return "GuauGuau....";
    }
    
    
    public function __construct() {
        $this->tipo="Perrito";
    }
}
