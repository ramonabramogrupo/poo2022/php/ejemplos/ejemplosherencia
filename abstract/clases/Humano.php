<?php

/**
 * Description of Humano
 *
 * @author Profesor Ramon
 */
class Humano extends Ser{
    private string $nombre;
    private int $edad;
    
    public function despedir(): string {
        return "adios";
    }

    public function saludar(): string {
        return "hola";
    }
    
    public function getNombre(): string {
        return $this->nombre;
    }

    public function getEdad(): int {
        return $this->edad;
    }

    public function setNombre(string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setEdad(int $edad): void {
        $this->edad = $edad;
    }

     
    public function __construct(string $pnombre,int $pedad) {
        $this->tipo ="Ser Humano";
        $this->nombre=$pnombre;
        $this->edad=$pedad;
    }
 

}
