<?php

/**
 * Description of Ser
 *
 * @author Profesor Ramon
 */
abstract class Ser {
    protected string $tipo;
    
    public function getTipo(): string {
        return $this->tipo;
    }

    public function setTipo(string $tipo): void {
        $this->tipo = $tipo;
    }
    
    abstract public function saludar() : string;
    abstract public function despedir() : string;
}
