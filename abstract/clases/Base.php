<?php

/**
 * Description of Base
 *
 * @author Profesor Ramon
 */
abstract class Base {
    public string $nombre;
    public string $fechaNacimiento;
    public string $apellidos;
    
    public function hablar():string{
        return "bla bla";
    }
    
    abstract public function presentacion():string;
}
