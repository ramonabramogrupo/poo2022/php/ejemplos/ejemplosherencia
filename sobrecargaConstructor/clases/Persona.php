<?php

class Persona {
    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    public function __construct(...$datos) {
        $numero=count($datos);
        $nombre="__construct{$numero}";
        if(method_exists($this, $nombre)){
            // call_user_func_array([$this,$nombre], $datos);
            $this->$nombre(...$datos);
        }
    }
    
    public function __construct0(){
        
    }
    
    public function __construct1(?string $nombre){
        $this->nombre = $nombre;
    }
    
    public function __construct2(?string $nombre, string $sexo){
        $this->nombre = $nombre;
        $this->sexo = $sexo;
    }
    
    public function __construct3(?string $nombre, string $sexo, int $edad) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }
    
    

}
