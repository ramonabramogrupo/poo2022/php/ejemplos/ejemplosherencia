<?php

class Persona_3 {
    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    public function __construct(array $datos) {
        // creamos un array asociativo con los valores
        // por defecto de las propiedades
//        $inicial=[
//          "nombre" => $this->nombre,
//          "sexo" => $this->sexo,
//          "edad" => $this->edad
//        ];
        
        // para crear el array anterior directamente
        $inicial=get_class_vars("Persona_3");
        
        // me quedo con los indices que me hayan pasado al      
        // constructor que existan como propiedad
        $final=array_intersect_key($datos,$inicial);
        
        // inicio todas las propiedades de la clase con
        // el array pasado
        foreach ($final as $indice=>$valor){
            $this->$indice=$valor;
        }  
        
        
        
        
        
        
    }    
    

}
