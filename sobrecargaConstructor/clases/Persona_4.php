<?php

class Persona_4 {
    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    public function __construct(array $datos) {
        
        // inicio todas las propiedades de la clase con
        // el array pasado
        foreach ($datos as $indice=>$valor){
            if(property_exists("Persona_4", $indice)){
                $this->$indice=$valor;
            }
   
        }  
        
    }    
}
