<?php

class Persona_1 {
    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    /**
     * voy a realizar la sobrecarga del constructor con parametros por defecto
     * 
     */
    public function __construct(?string $nombre=null, string $sexo='H', int $edad=0) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->edad = $edad;
    }

    

}
