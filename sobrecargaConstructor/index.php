<?php
// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$persona1=new Persona_4([
        "edad" => 25,
        "nombre" => "Ramon",
        "sexo" =>"H"
    ]);

var_dump($persona1);

$persona2=new Persona_4([]);
var_dump($persona2);

$persona3=new Persona_4([
        "nombre" => "Eva",
        "apellidos" => "Gomez"
    ]);

var_dump($persona3);