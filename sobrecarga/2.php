<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$a=new Persona_2();
$a->nombre="Jose";
$a->edad=20;

echo $a->saludar();
echo "<br>";
echo $a->saludar("Ramon");
echo "<br>";
echo $a->saludar("Ramon",45);
