<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$coche1=new Automovil();

echo $coche1->velocidad; // 0
echo "<br>";
$coche1->acelerar0(); 
echo $coche1->velocidad; // 1
echo "<br>";

$coche1->acelerar1(20);
echo $coche1->velocidad; // 21
echo "<br>";

$coche1->acelerar2(10,2);
echo $coche1->velocidad; // 21 + (10+1*2) = 33
echo "<br>";


$coche2=new Automovil_2();

echo $coche2->velocidad; // 0
echo "<br>";
$coche2->acelerar(); 
echo $coche2->velocidad; // 1
echo "<br>";

$coche2->acelerar(20);
echo $coche2->velocidad; // 21
echo "<br>";

$coche2->acelerar(10,2);
echo $coche2->velocidad; // 21 + (10+1*2) = 33
echo "<br>";
