<?php
// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$p=new Persona();
$p->nombre="Eva";
$p->sexo='M';
$p->edad=25;

var_dump($p);

echo $p->saludar();
echo "<br>";
echo $p->saludar1("ramon");
echo "<br>";
echo $p->saludar2("ramon",50);
echo "<br>";
/// 

$p1=new Persona_1();
$p1->nombre="Eva";
$p1->sexo='M';
$p1->edad=25;

echo $p1->saludar();
echo "<br>";
echo $p1->saludar("ramon");
echo "<br>";
echo $p1->saludar("ramon",50);
echo "<br>";