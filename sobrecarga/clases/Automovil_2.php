<?php

class Automovil_2 {
    public int $potencia=1;
    public int $velocidad=0;
    
    public function acelerar(...$datos){
        $numero=count($datos);
        $nombre="acelerar{$numero}";
        if(method_exists($this,$nombre)){
            $this->$nombre(...$datos);
            // call_user_func_array([$this,$nombre], $datos);
        }
    }
    
    private function acelerar0() : void{
        $this->velocidad++;
    }
    
    private function acelerar1(int $incremento) : void{
        $this->velocidad+=$incremento;
        // $this->velocidad=$this->velocidad+$incremento;
    }
    
    private function acelerar2(int $incremento,int $multiplo) : void{
        $this->velocidad+=($incremento+$this->potencia*$multiplo);
        //$this->velocidad=$this->velocidad+($incremento+$this->potencia*$multiplo);
    }
}
