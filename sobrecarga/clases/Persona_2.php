<?php

class Persona_2 {

    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    public function saludar(...$datos){
        $numero=count($datos);
        $nombre="saludar{$numero}";
        if(method_exists($this, $nombre)){
            //return call_user_func_array([$this,$nombre],$datos);
            return $this->$nombre(...$datos);
        }
//        if(count($datos)==0){
//            return $this->saludar0();
//        }
//        if(count($datos)==1){
//            return $this->saludar1(...$datos);
//        }
//        if(count($datos)==2){
//            return $this->saludar2(...$datos);
//        }
        
    }
    
        
    private function saludar0(){
        return "hola";
    }
    
    private function saludar1($a1){
        return "hola {$a1} yo soy {$this->nombre}";
    }
    
    private function saludar2($a1,$a2){
        return "hola {$a1} yo soy {$this->nombre}. Veo que tienes {$a2} años yo tengo {$this->edad}";
    }
    
}
