<?php

class Persona_1 {

    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    public function __call($name, $arguments) {
        $numero=count($arguments); // numero de argumentos pasados al metodo
        $nombre="{$name}{$numero}"; // creo el nombre del metodo en funcion del numero de argumentos pasados
        if(method_exists($this,$nombre)){ // comnprobando que el metodo exista
            // utilizando la funcion call_user_func_array
            // return call_user_func_array([$this,$nombre],$arguments);
            
            // utilizando una funcion variable
            return $this->$nombre(...$arguments);
            
        }
    }
            
    private function saludar0(){
        return "hola";
    }
    
    private function saludar1($a1){
        return "hola {$a1} yo soy {$this->nombre}";
    }
    
    private function saludar2($a1,$a2){
        return "hola {$a1} yo soy {$this->nombre}. Veo que tienes {$a2} años yo tengo {$this->edad}";
    }
    
}
