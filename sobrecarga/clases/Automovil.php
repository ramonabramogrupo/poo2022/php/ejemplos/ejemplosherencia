<?php

class Automovil {
    public int $potencia=1;
    public int $velocidad=0;
    
    public function acelerar0() : void{
        $this->velocidad++;
    }
    
    public function acelerar1(int $incremento) : void{
        $this->velocidad+=$incremento;
        // $this->velocidad=$this->velocidad+$incremento;
    }
    
    public function acelerar2(int $incremento,int $multiplo) : void{
        $this->velocidad+=($incremento+$this->potencia*$multiplo);
        //$this->velocidad=$this->velocidad+($incremento+$this->potencia*$multiplo);
    }
}
