<?php

class Automovil_1 {

    public int $potencia = 1;
    public int $velocidad = 0;

    public function __call($name, $arguments) {
        $numero = count($arguments);
        $nombre = "{$name}{$numero}";
        if (method_exists($this, $nombre)) {
            call_user_func_array([$this, $nombre], $arguments);
            //$this->$nombre(...$arguments);
        }
    }

    private function acelerar0(): void {
        $this->velocidad++;
    }

    private function acelerar1(int $incremento): void {
        $this->velocidad += $incremento;
        // $this->velocidad=$this->velocidad+$incremento;
    }

    private function acelerar2(int $incremento, int $multiplo): void {
        $this->velocidad += ($incremento + $this->potencia * $multiplo);
        //$this->velocidad=$this->velocidad+($incremento+$this->potencia*$multiplo);
    }

}
