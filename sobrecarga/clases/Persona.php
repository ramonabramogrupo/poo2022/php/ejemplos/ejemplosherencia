<?php

class Persona {

    public ?string $nombre=null;
    public string $sexo='H';
    public int $edad=0;
    
    public function saludar(){
        return "hola";
    }
    
    public function saludar1($a1){
        return "hola {$a1} yo soy {$this->nombre}";
    }
    
    public function saludar2($a1,$a2){
        return "hola {$a1} yo soy {$this->nombre}. Veo que tienes {$a2} años yo tengo {$this->edad}";
    }
    
}
